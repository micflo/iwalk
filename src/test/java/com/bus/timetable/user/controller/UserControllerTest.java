package com.bus.timetable.user.controller;

import com.bus.timetable.user.dto.UserDto;
import com.bus.timetable.user.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getUser_shouldReturnOneUser_whenDoesExist() throws Exception {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());

        var response = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("lastname").isNotEmpty())
                .andExpect(jsonPath("mail").isNotEmpty())
                .andExpect(jsonPath("dateOfBirth").isNotEmpty())
                .andExpect(jsonPath("enabled").isNotEmpty())
                .andReturn().getResponse().getContentAsString();
        var id = objectMapper.readValue(response, UserDto.class).getId();

        mockMvc.perform(get("/api/v1/users/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("lastname").isNotEmpty())
                .andExpect(jsonPath("mail").isNotEmpty())
                .andExpect(jsonPath("dateOfBirth").isNotEmpty())
                .andExpect(jsonPath("enabled").isNotEmpty());
    }

    @Test
    void deleteUser_shouldDeleteUser_whenDoesExist() throws Exception {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());

        var response = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("lastname").isNotEmpty())
                .andExpect(jsonPath("mail").isNotEmpty())
                .andExpect(jsonPath("dateOfBirth").isNotEmpty())
                .andExpect(jsonPath("enabled").isNotEmpty())
                .andReturn().getResponse().getContentAsString();
        var id = objectMapper.readValue(response, UserDto.class).getId();

        mockMvc.perform(delete("/api/v1/users/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void createUser_shouldCreateUser_whenMandatoryFieldsDoExist() throws Exception {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());

        mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("lastname").isNotEmpty())
                .andExpect(jsonPath("mail").isNotEmpty())
                .andExpect(jsonPath("dateOfBirth").isNotEmpty())
                .andExpect(jsonPath("enabled").isNotEmpty());
    }

    @Test
    void createUser_shouldThrowError_whenMandatoryFieldIsMissing() throws Exception {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setDateOfBirth(new Date());

        mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void getUsers_shouldReturnPageableResult_whenAnyExist() throws Exception {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());

        mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isCreated());

        var user2 = new User();
        user2.setName("new name");
        user2.setLastname("lastname");
        user2.setMail("mail2@gmail.com");
        user2.setPassword("new password");
        user2.setDateOfBirth(new Date());

        mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user2)))
                .andDo(print())
                .andExpect(status().isCreated());

        var user3 = new User();
        user3.setName("new name");
        user3.setLastname("lastname");
        user3.setMail("mail3@gmail.com");
        user3.setPassword("new password");
        user3.setDateOfBirth(new Date());

        mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user3)))
                .andDo(print())
                .andExpect(status().isCreated());


        mockMvc.perform(get("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("content[*].id").isNotEmpty())
                .andExpect(jsonPath("content[*].created").isNotEmpty())
                .andExpect(jsonPath("content[*].stamp").isNotEmpty())
                .andExpect(jsonPath("content[*].name").isNotEmpty())
                .andExpect(jsonPath("content[*].lastname").isNotEmpty())
                .andExpect(jsonPath("content[*].mail").isNotEmpty())
                .andExpect(jsonPath("content[*].dateOfBirth").isNotEmpty())
                .andExpect(jsonPath("content[*].enabled").isNotEmpty());
    }
}