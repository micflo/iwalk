package com.bus.timetable.user.service;

import com.bus.timetable.authority.model.Role;
import com.bus.timetable.authority.service.RoleDispatcherService;
import com.bus.timetable.authority.service.RoleService;
import com.bus.timetable.badge.model.Badge;
import com.bus.timetable.badge.service.AdvanceBadgeService;
import com.bus.timetable.badge.service.BadgeService;
import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.user.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private BadgeService badgeService;
    @Autowired
    private AdvanceBadgeService advanceBadgeService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleDispatcherService roleDispatcherService;

    @Test
    void save_shouldSaveUser_whenDataAreCorrect() {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());
        userService.save(user);

        assertNotNull(user.getStamp());
        assertNotNull(user.getCreated());

        userService.delete(user.getId());
    }

    @Test
    void delete_shouldThrowError_whenUserDoesNotExist() {
        var badId = 0L;
        assertThrows(NotFoundException.class,
                () -> userService.delete(badId)
        );
    }

    @Test
    void delete_shouldDeleteUserAndDetachUserFromBadge_whenBadgeIsAttachToUser() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var tatraBadge = new Badge();
        tatraBadge.setName("name");
        tatraBadge.setThreshold(300);
        tatraBadge.setDescription("Badge for hiking");
        badgeService.save(tatraBadge);

        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), john.getId());
        userService.delete(john.getId());
        var actualTatraBadge = badgeService.getBadgeById(tatraBadge.getId())
                .orElseThrow();

        var expectedBadgeUser = 0L;

        assertEquals(expectedBadgeUser, actualTatraBadge.getUsersOfSize());
        badgeService.delete(tatraBadge.getId());
    }

    @Test
    void delete_shouldDeleteOneUserAndDetachUserFromBadge_whenBadgeIsAttachToUser() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var michael = new User();
        michael.setName("new name");
        michael.setLastname("lastname");
        michael.setMail("mail2@gmail.com");
        michael.setPassword("new password");
        michael.setDateOfBirth(new Date());
        userService.save(michael);

        var tatraBadge = new Badge();
        tatraBadge.setName("name");
        tatraBadge.setThreshold(300);
        tatraBadge.setDescription("Badge for hiking");
        badgeService.save(tatraBadge);

        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), john.getId());
        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), michael.getId());
        userService.delete(michael.getId());

        var actualTatraBadge = badgeService.getBadgeById(tatraBadge.getId())
                .orElseThrow();

        var expectedUserCount = 1L;

        assertEquals(expectedUserCount, actualTatraBadge.getUsersOfSize());

        userService.delete(john.getId());
        badgeService.delete(tatraBadge.getId());
    }

    @Test
    void delete_shouldDeleteUserAndDetachUserFromRole_whenRoleIsAttachToUser() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);


        var role = new Role();
        role.setName("ADMIN");
        roleService.save(role);

        roleDispatcherService.attachRoleToUser(role.getId(), john.getId());
        userService.delete(john.getId());

        var actualRole = roleService.getRoleById(role.getId())
                .orElseThrow();

        var expectedUserCount = 0L;

        assertEquals(expectedUserCount, actualRole.getUsersOfSize());

        roleService.delete(role.getId());
    }

    @Test
    void delete_shouldDeleteOneUserAndDetachUserFromRole_whenRoleIsAttachToUser() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var michael = new User();
        michael.setName("new name");
        michael.setLastname("lastname");
        michael.setMail("mail2@gmail.com");
        michael.setPassword("new password");
        michael.setDateOfBirth(new Date());
        userService.save(michael);


        var role = new Role();
        role.setName("ADMIN");
        roleService.save(role);

        roleDispatcherService.attachRoleToUser(role.getId(), john.getId());
        roleDispatcherService.attachRoleToUser(role.getId(), michael.getId());
        userService.delete(john.getId());

        var actualRole = roleService.getRoleById(role.getId())
                .orElseThrow();

        var expectedUserCount = 1L;

        assertEquals(expectedUserCount, actualRole.getUsersOfSize());

        roleService.delete(role.getId());
        userService.delete(michael.getId());
    }
}