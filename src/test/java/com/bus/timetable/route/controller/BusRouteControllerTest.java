package com.bus.timetable.route.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class BusRouteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getRoutes_shouldReturnListOfAvailableRoutes_whenAnyExist() throws Exception {
        mockMvc.perform(get("/api/v1/routes")
                .param("startLng", "19.984691599999998")
                .param("startLat", "50.0371659")
                .param("endLng", "19.983530044555668")
                .param("endLat", "50.045096122877816"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("code").isString())
                .andExpect(jsonPath("routes").isArray())
                .andExpect(jsonPath("routes[0].distance").isNumber())
                .andExpect(jsonPath("routes[0].duration").isNumber())
                .andExpect(jsonPath("routes[0].legs").isArray())
                .andExpect(jsonPath("routes[0].legs[0].steps").isArray())
                .andExpect(jsonPath("routes[0].legs[0].steps[0].distance").isNumber())
                .andExpect(jsonPath("routes[0].legs[0].steps[0].duration").isNumber())
                .andExpect(jsonPath("routes[0].legs[0].steps[0].geometry").isMap())
                .andExpect(jsonPath("routes[0].legs[0].steps[0].geometry.coordinates").isArray())
                .andExpect(jsonPath("routes[0].legs[0].steps[0].geometry.coordinates[0]").isArray())
                .andExpect(jsonPath("routes[0].legs[0].steps[0].geometry.coordinates[0][0]").isNumber());
    }
}