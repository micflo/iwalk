package com.bus.timetable.authority.controller;

import com.bus.timetable.authority.model.Role;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class RoleControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void complexTest_shouldCreateGetAndDeleteRole() throws Exception {
        var role = new Role();
        role.setName("ADMIN");

        var response = mockMvc.perform(post("/api/v1/roles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(role)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        var id = objectMapper.readValue(response, Role.class).getId();

        mockMvc.perform(get("/api/v1/roles/" + id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty());

        mockMvc.perform(get("/api/v1/roles"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("content[*].id").isNotEmpty())
                .andExpect(jsonPath("content[*].created").isNotEmpty())
                .andExpect(jsonPath("content[*].stamp").isNotEmpty())
                .andExpect(jsonPath("content[*].name").isNotEmpty());

        mockMvc.perform(delete("/api/v1/roles/" + id))
                .andExpect(status().isNoContent());
    }
}