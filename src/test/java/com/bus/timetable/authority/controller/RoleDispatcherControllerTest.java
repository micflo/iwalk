package com.bus.timetable.authority.controller;

import com.bus.timetable.authority.model.Role;
import com.bus.timetable.user.dto.UserDto;
import com.bus.timetable.user.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class RoleDispatcherControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void roleDispatcherComplexTest_shouldAttachAndDetachRoleToUser_whenRoleAndUserDoesExist() throws Exception {
        var role = new Role();
        role.setName("ADMIN");

        var roleResponse = mockMvc.perform(post("/api/v1/roles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(role)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        var roleId = objectMapper.readValue(roleResponse, Role.class).getId();

        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());

        var userResponse = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        var userId = objectMapper.readValue(userResponse, UserDto.class).getId();

        mockMvc.perform(put("/api/v1/roles/" + roleId + "/users/" + userId + "/attach")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        mockMvc.perform(put("/api/v1/roles/" + roleId + "/users/" + userId + "/detach")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        mockMvc.perform(delete("/api/v1/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        mockMvc.perform(delete("/api/v1/roles/" + roleId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}