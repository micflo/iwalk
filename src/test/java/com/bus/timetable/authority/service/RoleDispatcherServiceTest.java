package com.bus.timetable.authority.service;

import com.bus.timetable.authority.model.Role;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
class RoleDispatcherServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleDispatcherService roleDispatcherService;

    @Test
    void attachRoleToUser_shouldAttachRoleToUser_whenUserAndRoleDoesExist() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);


        var role = new Role();
        role.setName("ADMIN");
        roleService.save(role);

        roleDispatcherService.attachRoleToUser(role.getId(), john.getId());

        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();

        var expectedRoleCount = 1L;

        assertEquals(expectedRoleCount, actualJohn.getRolesOfSize());

        userService.delete(john.getId());
        roleService.delete(role.getId());
    }

    @Test
    void detachRoleToUser_shouldDetachRoleFromUser_whenUserAndRoleDoesExist() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);


        var role = new Role();
        role.setName("ADMIN");
        roleService.save(role);

        roleDispatcherService.attachRoleToUser(role.getId(), john.getId());
        roleDispatcherService.detachRoleToUser(role.getId(), john.getId());

        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();

        var expectedRoleCount = 0L;

        assertEquals(expectedRoleCount, actualJohn.getRolesOfSize());

        userService.delete(john.getId());
        roleService.delete(role.getId());
    }
}