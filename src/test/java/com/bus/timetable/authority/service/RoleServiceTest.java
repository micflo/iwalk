package com.bus.timetable.authority.service;

import com.bus.timetable.authority.model.Role;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
class RoleServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleDispatcherService roleDispatcherService;

    @Test
    void delete_shouldDeleteRoleAndDetachRoleFromUser_whenIsAttached() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var michael = new User();
        michael.setName("new name");
        michael.setLastname("lastname");
        michael.setMail("mail2@gmail.com");
        michael.setPassword("new password");
        michael.setDateOfBirth(new Date());
        userService.save(michael);


        var role = new Role();
        role.setName("ADMIN");
        roleService.save(role);

        roleDispatcherService.attachRoleToUser(role.getId(), john.getId());
        roleDispatcherService.attachRoleToUser(role.getId(), michael.getId());
        roleService.delete(role.getId());

        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();

        var actualMichael = userService.getUserById(michael.getId())
                .orElseThrow();

        var expectedRoleCount = 0L;

        assertEquals(expectedRoleCount, actualJohn.getRolesOfSize());
        assertEquals(expectedRoleCount, actualMichael.getRolesOfSize());

        userService.delete(michael.getId());
        userService.delete(john.getId());
    }

    @Test
    void delete_shouldDeleteOneRoleAndDetachRoleFromUser_whenIsAttached() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var michael = new User();
        michael.setName("new name");
        michael.setLastname("lastname");
        michael.setMail("mail2@gmail.com");
        michael.setPassword("new password");
        michael.setDateOfBirth(new Date());
        userService.save(michael);


        var admin = new Role();
        admin.setName("ADMIN");
        roleService.save(admin);


        var user = new Role();
        user.setName("USER");
        roleService.save(user);

        roleDispatcherService.attachRoleToUser(admin.getId(), john.getId());
        roleDispatcherService.attachRoleToUser(user.getId(), michael.getId());
        roleService.delete(admin.getId());

        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();

        var actualMichael = userService.getUserById(michael.getId())
                .orElseThrow();

        var expectedJohnRoleCount = 0L;
        var expectedMichaelRoleCount = 1L;

        assertEquals(expectedJohnRoleCount, actualJohn.getRolesOfSize());
        assertEquals(expectedMichaelRoleCount, actualMichael.getRolesOfSize());

        userService.delete(michael.getId());
        userService.delete(john.getId());
        roleService.delete(user.getId());
    }
}