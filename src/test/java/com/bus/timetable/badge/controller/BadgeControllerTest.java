package com.bus.timetable.badge.controller;

import com.bus.timetable.badge.dto.BadgeDto;
import com.bus.timetable.badge.dto.BadgePostDto;
import com.bus.timetable.badge.dto.BadgePutDto;
import com.bus.timetable.user.dto.UserDto;
import com.bus.timetable.user.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class BadgeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getBadge_shouldReturnOneBadge_whenExist() throws Exception {
        var badgePostDto = new BadgePostDto();
        badgePostDto.setDescription("Badge for hiking");
        badgePostDto.setName("name");
        badgePostDto.setThreshold(500);


        var response = mockMvc.perform(post("/api/v1/badges")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(badgePostDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("threshold").isNotEmpty())
                .andExpect(jsonPath("description").isNotEmpty())
                .andReturn().getResponse().getContentAsString();


        var id = objectMapper.readValue(response, BadgeDto.class).getId();

        mockMvc.perform(get("/api/v1/badges/" + id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("threshold").isNotEmpty())
                .andExpect(jsonPath("description").isNotEmpty());

        mockMvc.perform(delete("/api/v1/badges/" + id))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    void createAndDeleteBadge_shouldSaveBadge_whenRequestIsCorrect() throws Exception {
        var badgePostDto = new BadgePostDto();
        badgePostDto.setDescription("Badge for hiking");
        badgePostDto.setName("name");
        badgePostDto.setThreshold(500);


        var response = mockMvc.perform(post("/api/v1/badges")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(badgePostDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("threshold").isNotEmpty())
                .andExpect(jsonPath("description").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        var id = objectMapper.readValue(response, BadgeDto.class).getId();

        mockMvc.perform(delete("/api/v1/badges/" + id))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    void getBadges_shouldReturnPageOfBadges_whenAnyExist() throws Exception {
        var badgePostDto = new BadgePostDto();
        badgePostDto.setDescription("Badge for hiking");
        badgePostDto.setName("name");
        badgePostDto.setThreshold(500);

        var response = mockMvc.perform(post("/api/v1/badges")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(badgePostDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("threshold").isNotEmpty())
                .andExpect(jsonPath("description").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        mockMvc.perform(get("/api/v1/badges"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("content[*].id").isNotEmpty())
                .andExpect(jsonPath("content[*].created").isNotEmpty())
                .andExpect(jsonPath("content[*].stamp").isNotEmpty())
                .andExpect(jsonPath("content[*].name").isNotEmpty())
                .andExpect(jsonPath("content[*].threshold").isNotEmpty())
                .andExpect(jsonPath("content[*].description").isNotEmpty());

        var id = objectMapper.readValue(response, BadgeDto.class).getId();

        mockMvc.perform(delete("/api/v1/badges/" + id))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    void updateBadge_shouldUpdateBadge_whenRequestIsCorrect() throws Exception {
        var badgePostDto = new BadgePostDto();
        badgePostDto.setDescription("Badge for hiking");
        badgePostDto.setName("name");
        badgePostDto.setThreshold(500);


        var response = mockMvc.perform(post("/api/v1/badges")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(badgePostDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("threshold").isNotEmpty())
                .andExpect(jsonPath("description").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        var id = objectMapper.readValue(response, BadgeDto.class).getId();

        var badgePutDto = new BadgePutDto();
        badgePutDto.setName("New name");

        mockMvc.perform(put("/api/v1/badges/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(badgePutDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("created").isNotEmpty())
                .andExpect(jsonPath("stamp").isNotEmpty())
                .andExpect(jsonPath("name").isNotEmpty())
                .andExpect(jsonPath("threshold").isNotEmpty())
                .andExpect(jsonPath("description").isNotEmpty());

        mockMvc.perform(delete("/api/v1/badges/" + id))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    @Transactional
    void getBadgesAttachedToUser_shouldReturnPageOfBadgesAttachedToUser_whenAnyExist() throws Exception {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());

        var userResponse = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        var userId = objectMapper.readValue(userResponse, UserDto.class).getId();


        var badgePostDto = new BadgePostDto();
        badgePostDto.setDescription("Badge for hiking");
        badgePostDto.setName("name");
        badgePostDto.setThreshold(500);

       var badgeResponse=  mockMvc.perform(post("/api/v1/badges")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(badgePostDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        var badgeId = objectMapper.readValue(badgeResponse, BadgeDto.class).getId();

        mockMvc.perform(put("/api/v1/badges/" + badgeId +"/users/" + userId + "/attach"))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/v1/badges/users/" + userId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("content[*].id").isNotEmpty())
                .andExpect(jsonPath("content[*].created").isNotEmpty())
                .andExpect(jsonPath("content[*].stamp").isNotEmpty())
                .andExpect(jsonPath("content[*].name").isNotEmpty())
                .andExpect(jsonPath("content[*].threshold").isNotEmpty())
                .andExpect(jsonPath("content[*].description").isNotEmpty());
    }
}