package com.bus.timetable.badge.controller;

import com.bus.timetable.badge.dto.BadgeDto;
import com.bus.timetable.badge.dto.BadgePostDto;
import com.bus.timetable.user.dto.UserDto;
import com.bus.timetable.user.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AdvanceBadgeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void advanceBadgeComplexTest_shouldAttachAndDetachBadgeToUser_whenBadgeAndUserDoesExist() throws Exception {
        var user = new User();
        user.setName("new name");
        user.setLastname("lastname");
        user.setMail("mail@gmail.com");
        user.setPassword("new password");
        user.setDateOfBirth(new Date());

        var userResponse = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        var userId = objectMapper.readValue(userResponse, UserDto.class).getId();


        var badgePostDto = new BadgePostDto();
        badgePostDto.setDescription("Badge for hiking");
        badgePostDto.setName("name");
        badgePostDto.setThreshold(500);

        var badgeResponse=  mockMvc.perform(post("/api/v1/badges")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(badgePostDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        var badgeId = objectMapper.readValue(badgeResponse, BadgeDto.class).getId();

        mockMvc.perform(put("/api/v1/badges/" + badgeId +"/users/" + userId + "/attach"))
                .andExpect(status().isNoContent());

        mockMvc.perform(put("/api/v1/badges/" + badgeId +"/users/" + userId + "/detach"))
                .andExpect(status().isNoContent());

        mockMvc.perform(delete("/api/v1/badges/" + badgeId))
                .andDo(print())
                .andExpect(status().isNoContent());

        mockMvc.perform(delete("/api/v1/users/" + userId))
                .andDo(print())
                .andExpect(status().isNoContent());
    }
}