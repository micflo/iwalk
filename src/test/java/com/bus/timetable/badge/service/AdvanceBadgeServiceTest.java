package com.bus.timetable.badge.service;

import com.bus.timetable.badge.model.Badge;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
class AdvanceBadgeServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private BadgeService badgeService;
    @Autowired
    private AdvanceBadgeService advanceBadgeService;

    @Test
    void attachBadgeToUser_shouldAttachOneBadgeToTwoUsers_whenBothEntityDoesExist() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var michael = new User();
        michael.setName("new name");
        michael.setLastname("lastname");
        michael.setMail("mail2@gmail.com");
        michael.setPassword("new password");
        michael.setDateOfBirth(new Date());
        userService.save(michael);

        var tatraBadge = new Badge();
        tatraBadge.setName("name");
        tatraBadge.setThreshold(300);
        tatraBadge.setDescription("Badge for hiking");
        badgeService.save(tatraBadge);

        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), john.getId());
        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), michael.getId());


        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();
        var actualMichael = userService.getUserById(michael.getId())
                .orElseThrow();
        var actualBadge = badgeService.getBadgeById(tatraBadge.getId())
                .orElseThrow();

        var expectedJohnBadgeCount = 1L;
        var expectedMichaelBadgeCount = 1L;
        var expectedBadgeUserCount = 2L;

        assertEquals(expectedJohnBadgeCount, actualJohn.getBadgesOfSize());
        assertEquals(expectedMichaelBadgeCount, actualMichael.getBadgesOfSize());
        assertEquals(expectedBadgeUserCount, actualBadge.getUsersOfSize());

        userService.delete(actualJohn.getId());
        userService.delete(actualMichael.getId());
        badgeService.delete(tatraBadge.getId());
    }

    @Test
    void detachBadgeFromUser_shouldDetachBadgeFromUser_whenWasAttached() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var michael = new User();
        michael.setName("new name");
        michael.setLastname("lastname");
        michael.setMail("mail2@gmail.com");
        michael.setPassword("new password");
        michael.setDateOfBirth(new Date());
        userService.save(michael);

        var tatraBadge = new Badge();
        tatraBadge.setName("name");
        tatraBadge.setThreshold(300);
        tatraBadge.setDescription("Badge for hiking");
        badgeService.save(tatraBadge);

        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), john.getId());
        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), michael.getId());
        advanceBadgeService.detachBadgeFromUser(tatraBadge.getId(), john.getId());
        advanceBadgeService.detachBadgeFromUser(tatraBadge.getId(), michael.getId());


        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();
        var actualMichael = userService.getUserById(michael.getId())
                .orElseThrow();
        var actualBadge = badgeService.getBadgeById(tatraBadge.getId())
                .orElseThrow();

        var expectedJohnBadgeCount = 0L;
        var expectedMichaelBadgeCount = 0L;
        var expectedBadgeUserCount = 0L;

        assertEquals(expectedJohnBadgeCount, actualJohn.getBadgesOfSize());
        assertEquals(expectedMichaelBadgeCount, actualMichael.getBadgesOfSize());
        assertEquals(expectedBadgeUserCount, actualBadge.getUsersOfSize());

        userService.delete(actualJohn.getId());
        userService.delete(actualMichael.getId());
        badgeService.delete(tatraBadge.getId());
    }

    @Test
    void detachBadgeFromUser_shouldDoNothing_whenWasntAttache() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var michael = new User();
        michael.setName("new name");
        michael.setLastname("lastname");
        michael.setMail("mail2@gmail.com");
        michael.setPassword("new password");
        michael.setDateOfBirth(new Date());
        userService.save(michael);

        var tatraBadge = new Badge();
        tatraBadge.setName("name");
        tatraBadge.setThreshold(300);
        tatraBadge.setDescription("Badge for hiking");
        badgeService.save(tatraBadge);

        advanceBadgeService.detachBadgeFromUser(tatraBadge.getId(), john.getId());
        advanceBadgeService.detachBadgeFromUser(tatraBadge.getId(), michael.getId());


        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();
        var actualMichael = userService.getUserById(michael.getId())
                .orElseThrow();
        var actualBadge = badgeService.getBadgeById(tatraBadge.getId())
                .orElseThrow();

        var expectedJohnBadgeCount = 0L;
        var expectedMichaelBadgeCount = 0L;
        var expectedBadgeUserCount = 0L;

        assertEquals(expectedJohnBadgeCount, actualJohn.getBadgesOfSize());
        assertEquals(expectedMichaelBadgeCount, actualMichael.getBadgesOfSize());
        assertEquals(expectedBadgeUserCount, actualBadge.getUsersOfSize());

        userService.delete(actualJohn.getId());
        userService.delete(actualMichael.getId());
        badgeService.delete(tatraBadge.getId());
    }
}