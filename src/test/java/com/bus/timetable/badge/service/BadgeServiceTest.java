package com.bus.timetable.badge.service;

import com.bus.timetable.badge.model.Badge;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
class BadgeServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private BadgeService badgeService;
    @Autowired
    private AdvanceBadgeService advanceBadgeService;

    @Test
    void delete_shouldDeleteBadgeAndDetachUserFromBadge_whenBadgeIsAttachToUser() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var tatraBadge = new Badge();
        tatraBadge.setName("name");
        tatraBadge.setThreshold(300);
        tatraBadge.setDescription("Badge for hiking");
        badgeService.save(tatraBadge);

        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), john.getId());
        badgeService.delete(tatraBadge.getId());
        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();

        var expectedBadgeUser = 0L;

        assertEquals(expectedBadgeUser, actualJohn.getBadgesOfSize());
        assertTrue(badgeService.getBadgeById(tatraBadge.getId()).isEmpty());

        userService.delete(john.getId());
    }

    @Test
    void delete_shouldDeleteOneBadgeAndDetachUserFromBadge_whenBadgeIsAttachToUser() {
        var john = new User();
        john.setName("new name");
        john.setLastname("lastname");
        john.setMail("mail@gmail.com");
        john.setPassword("new password");
        john.setDateOfBirth(new Date());
        userService.save(john);

        var tatraBadge = new Badge();
        tatraBadge.setName("name");
        tatraBadge.setThreshold(300);
        tatraBadge.setDescription("Badge for hiking");
        badgeService.save(tatraBadge);


        var mountainBadge = new Badge();
        mountainBadge.setName("name2");
        mountainBadge.setThreshold(1300);
        mountainBadge.setDescription("Badge for hiking");
        badgeService.save(mountainBadge);

        advanceBadgeService.attachBadgeToUser(tatraBadge.getId(), john.getId());
        advanceBadgeService.attachBadgeToUser(mountainBadge.getId(), john.getId());
        badgeService.delete(mountainBadge.getId());
        var actualJohn = userService.getUserById(john.getId())
                .orElseThrow();

        var actualJohnBadges = badgeService.getBadgesAttachedToUser(john.getId(), PageRequest.of(0, 1))
                .get()
                .collect(toList());
        var expectedBadgeUser = 1L;

        assertEquals(expectedBadgeUser, actualJohn.getBadgesOfSize());
        assertEquals(actualJohnBadges.get(0).getName(), tatraBadge.getName());
        assertTrue(badgeService.getBadgeById(mountainBadge.getId()).isEmpty());

        userService.delete(john.getId());
        badgeService.delete(tatraBadge.getId());
    }
}