package com.bus.timetable.busStop.service;

import com.bus.timetable.busStop.model.BusStop;
import com.bus.timetable.busStop.dto.Point;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class BusStopServiceTest {

    @Autowired
    private BusStopService busStopService;
    @Autowired
    private BusStopSegmentService busStopSegmentService;

    // TODO data provider for tests.
    @Test
    void save() {
//        startLng=19.98299360275269&startLat=
//        MATCH (start:BusStop)-[:NEXT_BUS_STOP]-> (end:BusStop)
//        WITH
//        min(round(distance(point({longitude: start.longitude, latitude: start.latitude}), point({longitude: 19.98299360275269, latitude: 50.040873066336125})))) as minStartRouteDistance,
//        min(round(distance(point({longitude: end.longitude, latitude: end.latitude}), point({longitude: 19.978626966476444, latitude: 50.04092819028941})))) as minEndRouteDistance
//        MATCH (start:BusStop)-[:NEXT_BUS_STOP]->(end:BusStop)
//        WITH
//        round(distance(point({longitude: start.longitude, latitude: start.latitude}), point({longitude: 19.98299360275269, latitude: 50.040873066336125})))  as routeStartDistance,
//        start as startBus,
//        round(distance(point({longitude: end.longitude, latitude: end.latitude}), point({longitude: 19.978626966476444, latitude: 50.04092819028941})))  as routeEndDistance,
//        end as endBusStop
//        WHERE routeStartDistance = minStartRouteDistance
//        AND  routeEndDistance = minEndRouteDistance
//        RETURN routeStartDistance, startBus, routeEndDistance, endBusStop
//
//        MATCH (end:BusStop)
//        WITH
//
//        MATCH (end:BusStop)
//        WITH
//
//        WHERE routeEndDistance = minEndRouteDistance
//        RETURN routeEndDistance, endBusStop

        var busStop = new BusStop();
        busStop.setName("Lipska");
        busStop.setLatitude(50.04065601515487f);
        busStop.setLongitude(19.98167932033539f);
        busStopService.save(busStop);

        var nextBusStop = new BusStop();
        nextBusStop.setLongitude(19.978101253509525f);
        nextBusStop.setLatitude(50.04063189829639f);
        nextBusStop.setName("Gromadzka");
        busStopService.save(nextBusStop);

        busStopSegmentService.createSegmentFromBusStops(busStop.getId(), nextBusStop.getId());

//        busStopService.delete(busStop.getId());
//        busStopService.delete(busStop.getNextBustStop().getId());
    }

    @Test
    void test() {

        Point startPoint = new Point();
        startPoint.setLongitude(19.98299360275269f);
        startPoint.setLatitude(50.040873066336125f);
        Point stopPoint = new Point();
        stopPoint.setLongitude(19.978626966476444f);
        stopPoint.setLatitude(50.04092819028941f);
        System.out.println(busStopService.getTwoNearestBusStopBaseOneStartAndStopPoint(startPoint, stopPoint).get());
        System.out.println("aa");
    }

    @Test
    void getBusStopById_shouldBeEmpty_whenNotExist() {
        var notExistedBusStopId = 1L;
        assertTrue(busStopService.getBusStopById(notExistedBusStopId).isEmpty());
    }

    @Test
    void delete() {
        busStopService.delete(2L);
        busStopService.delete(1L);
        busStopService.delete(0L);
    }
}