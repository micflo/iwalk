package com.bus.timetable.busStop.service;

import com.bus.timetable.busStop.model.BusStop;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class BusRouteServiceTest {

    @Autowired
    private BusRouteService busRouteService;
    @Autowired
    private BusStopService busStopService;


    @Test
    public void createBusRoute_shouldCreateBusRoute_whenFieldAreProper() {

        var busStop = new BusStop();
        busStop.setName("Bus stop");
        busStop.setLatitude(44.01);
        busStop.setLongitude(33.02);
        busStopService.save(busStop);

        var nextBusStop = new BusStop();
        nextBusStop.setLongitude(55.01);
        nextBusStop.setLatitude(65.01);
        nextBusStop.setName("Next Bus stop");
        busStopService.save(nextBusStop);


        var beforeLast = new BusStop();
        beforeLast.setLongitude(55.01);
        beforeLast.setLatitude(65.01);
        beforeLast.setName("Before last Bus stop");
        busStopService.save(beforeLast);

//
        var lastBusStop = new BusStop();
        lastBusStop.setLongitude(55.01);
        lastBusStop.setLatitude(65.01);
        lastBusStop.setName("Last Bus stop");
        busStopService.save(lastBusStop);

//        busRoute.setBusStops(List.of(busStop, nextBusStop, beforeLast, busStop1));
//        busRouteService.createBusRoute(busRoute);

        System.out.println("dupa");
    }
}