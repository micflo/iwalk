package com.bus.timetable.authority.controller;

import com.bus.timetable.authority.service.RoleDispatcherService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/roles/{roleId}/users/{userId}")
public class RoleDispatcherController {

    private final RoleDispatcherService roleDispatcherService;

    @PutMapping("attach")
    public ResponseEntity<Void> attachRoleToUser(@PathVariable Long roleId, @PathVariable Long userId) {
        roleDispatcherService.attachRoleToUser(roleId, userId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("detach")
    public ResponseEntity<Void> detachRoleToUser(@PathVariable Long roleId, @PathVariable Long userId) {
        roleDispatcherService.detachRoleToUser(roleId, userId);
        return ResponseEntity.noContent().build();
    }
}
