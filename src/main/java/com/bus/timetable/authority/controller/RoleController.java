package com.bus.timetable.authority.controller;

import com.bus.timetable.authority.dto.RoleDto;
import com.bus.timetable.authority.dto.RolePostDto;
import com.bus.timetable.authority.model.Role;
import com.bus.timetable.authority.service.RoleService;
import com.bus.timetable.exception.NotFoundException;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/roles")
public class RoleController {

    private final RoleService roleService;
    private final ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<Page<RoleDto>> getRoles(Pageable pageable) {
        return ResponseEntity
                .ok(roleService.getRoles(pageable)
                        .map(role -> modelMapper.map(role, RoleDto.class)));
    }

    @GetMapping("{roleId}")
    public ResponseEntity<RoleDto> getRole(@PathVariable Long roleId) {
        return roleService.getRoleById(roleId)
                .map(role -> modelMapper.map(role, RoleDto.class))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NotFoundException("Nie znaleziono podanej roli."));
    }

    @DeleteMapping("{roleId}")
    public ResponseEntity<Void> deleteRole(@PathVariable Long roleId) {
        roleService.delete(roleId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public ResponseEntity<RoleDto> createRole(@RequestBody RolePostDto rolePostDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(modelMapper.map(roleService.save(modelMapper.map(rolePostDto, Role.class)), RoleDto.class));
    }
}
