package com.bus.timetable.authority.model;


import com.bus.timetable.audit.DateAudit;
import com.bus.timetable.user.model.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Formula;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Setter
@Getter
public class Role extends DateAudit {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Length(max = 25)
    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users = new HashSet<>();

    @Formula("(SELECT COUNT(*) FROM USER_ROLES ur WHERE ur.ROLE_ID = id)")
    private Long usersOfSize;

    public void excludeRoleFromUsers() {
        users.forEach(user -> user.getRoles().remove(this));
    }
}
