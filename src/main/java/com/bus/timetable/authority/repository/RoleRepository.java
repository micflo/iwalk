package com.bus.timetable.authority.repository;

import com.bus.timetable.authority.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
