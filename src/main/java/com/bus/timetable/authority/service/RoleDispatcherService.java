package com.bus.timetable.authority.service;

import com.bus.timetable.authority.model.Role;
import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.BiConsumer;

@Service
@Transactional
@AllArgsConstructor
public class RoleDispatcherService {

    private final UserService userService;
    private final RoleService roleService;

    public void attachRoleToUser(Long roleId, Long userId) {
        makeOperation(roleId, userId, (role, user) -> {
            role.getUsers().add(user);
            user.getRoles().add(role);
        });
    }

    public void detachRoleToUser(Long roleId, Long userId) {
        makeOperation(roleId, userId, (role, user) -> {
            role.getUsers().remove(user);
            user.getRoles().remove(role);
        });
    }

    private void makeOperation(Long roleId, Long userId, BiConsumer<Role, User> userRoleBiConsumer) {
        var role = roleService.getRoleById(roleId)
                .orElseThrow(() -> new NotFoundException("Nie znalezino podanej roli"));

        var user = userService.getUserById(userId)
                .orElseThrow(() -> new NotFoundException("Nie znaleziono podanego użytkownika."));

        userRoleBiConsumer.accept(role, user);
    }
}
