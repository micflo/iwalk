package com.bus.timetable.authority.service;

import com.bus.timetable.authority.model.Role;
import com.bus.timetable.authority.repository.RoleRepository;
import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.exception.service.Handler;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;
    private final Handler<Role, RoleRepository> roleRepositoryHandler;

    public Optional<Role> getRoleById(Long roleId) {
        return roleRepository.findById(roleId);
    }

    public Page<Role> getRoles(Pageable pageable) {
        return roleRepository.findAll(pageable);
    }

    public Role save(Role role) {
        return roleRepositoryHandler.trySave(role, roleRepository);
    }

    public void delete(Long roleId) {
        getRoleById(roleId)
                .orElseThrow(() -> new NotFoundException("Nie znaleziono podanego użytkownika."))
                .excludeRoleFromUsers();

        roleRepository.deleteById(roleId);
    }
}
