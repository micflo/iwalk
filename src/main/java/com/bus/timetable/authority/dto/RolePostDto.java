package com.bus.timetable.authority.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RolePostDto {

    private String name;
}
