package com.bus.timetable.authority.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
public class RoleDto {

    private Long id;
    private String name;
    private OffsetDateTime created;
    private OffsetDateTime stamp;
}
