package com.bus.timetable.audit.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.neo4j.annotation.EnableNeo4jAuditing;

import java.time.OffsetDateTime;
import java.util.Optional;

@Configuration
@EnableJpaAuditing(dateTimeProviderRef = "createOffsetDateTime")
@EnableNeo4jAuditing(dateTimeProviderRef = "createOffsetDateTime")
public class DateConfiguration {

    @Bean
    public DateTimeProvider createOffsetDateTime() {
        return () -> Optional.of(OffsetDateTime.now());
    }
}
