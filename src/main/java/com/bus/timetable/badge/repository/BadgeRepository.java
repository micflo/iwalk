package com.bus.timetable.badge.repository;

import com.bus.timetable.badge.model.Badge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BadgeRepository extends JpaRepository<Badge, Long> {
    Page<Badge> findBadgesByUsersId(Long userId, Pageable pageable);
}
