package com.bus.timetable.badge.controller;

import com.bus.timetable.badge.dto.BadgeDto;
import com.bus.timetable.badge.dto.BadgePostDto;
import com.bus.timetable.badge.dto.BadgePutDto;
import com.bus.timetable.badge.model.Badge;
import com.bus.timetable.badge.service.BadgeService;
import com.bus.timetable.exception.NotFoundException;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/badges")
public class BadgeController {

    private final BadgeService badgeService;
    private final ModelMapper modelMapper;

    @GetMapping("{badgeId}")
    public ResponseEntity<BadgeDto> getBadge(@PathVariable Long badgeId) {
        return badgeService.getBadgeById(badgeId)
                .map(badge -> modelMapper.map(badge, BadgeDto.class))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NotFoundException("Podana odznaka nie istnieje."));
    }

    @GetMapping
    public ResponseEntity<Page<BadgeDto>> getBadges(Pageable pageable) {
        return ResponseEntity.ok(badgeService.getBadges(pageable)
                .map(badge -> modelMapper.map(badge, BadgeDto.class)));
    }

    @GetMapping("users/{userId}")
    public ResponseEntity<Page<BadgeDto>> getBadgesAttachedToUser(@PathVariable Long userId, Pageable pageable) {
        return ResponseEntity.ok(
                badgeService.getBadgesAttachedToUser(userId, pageable)
                .map(badge -> modelMapper.map(badge, BadgeDto.class)));
    }

    @PostMapping
    public ResponseEntity<BadgeDto> createBadge(@RequestBody BadgePostDto badgePostDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(modelMapper.map( badgeService.save(modelMapper.map(badgePostDto, Badge.class)), BadgeDto.class));
    }

    @PutMapping("{badgeId}")
    public ResponseEntity<BadgeDto> updateBadge(@PathVariable Long badgeId, @RequestBody BadgePutDto badgePutDto) {
        return ResponseEntity
                .ok(modelMapper.map(badgeService.update(badgeId, modelMapper.map(badgePutDto, Badge.class)), BadgeDto.class));
    }

    @DeleteMapping("{badgeId}")
    public ResponseEntity<Void> deleteBadge(@PathVariable Long badgeId) {
        badgeService.delete(badgeId);
        return ResponseEntity.noContent().build();
    }
}
