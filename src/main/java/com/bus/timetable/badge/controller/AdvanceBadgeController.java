package com.bus.timetable.badge.controller;

import com.bus.timetable.badge.service.AdvanceBadgeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/badges/{badgeId}/users/{userId}")
public class AdvanceBadgeController {

    private final AdvanceBadgeService advanceBadgeService;

    @PutMapping("attach")
    public ResponseEntity<Void> attachBadgeToUser(@PathVariable Long badgeId, @PathVariable Long userId) {
        advanceBadgeService.attachBadgeToUser(badgeId, userId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("detach")
    public ResponseEntity<Void> detachBadgeToUser(@PathVariable Long badgeId, @PathVariable Long userId) {
        advanceBadgeService.detachBadgeFromUser(badgeId, userId);
        return ResponseEntity.noContent().build();
    }
}
