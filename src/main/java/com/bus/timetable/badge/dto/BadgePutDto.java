package com.bus.timetable.badge.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BadgePutDto {

    private String name;
    private Integer threshold;
    private String description;
}
