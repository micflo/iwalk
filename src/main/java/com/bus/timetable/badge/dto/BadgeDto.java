package com.bus.timetable.badge.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
public class BadgeDto {

    private Long id;
    private OffsetDateTime created;
    private OffsetDateTime stamp;
    private String name;
    private Integer threshold;
    private String description;
}
