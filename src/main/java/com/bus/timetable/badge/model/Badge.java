package com.bus.timetable.badge.model;

import com.bus.timetable.audit.DateAudit;
import com.bus.timetable.user.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Formula;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Badge extends DateAudit {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Length(max = 50)
    private String name;

    @NotNull
    private Integer threshold;

    @NotNull
    @Length(max = 1000)
    private String description;

    @ManyToMany(mappedBy = "badges")
    private Set<User> users = new HashSet<>();

    @Formula("SELECT COUNT(*) FROM USER_BADGES ub WHERE ub.badge_id = id")
    private Long usersOfSize;

    public void deleteBadgeFromUsers() {
        users.forEach(user -> user.getBadges().remove(this));
    }

    public void excludeUser(User user) {
        users.remove(user);
    }
}
