package com.bus.timetable.badge.service;

import com.bus.timetable.badge.model.Badge;
import com.bus.timetable.badge.repository.BadgeRepository;
import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.exception.service.Handler;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class BadgeService {

    private final BadgeRepository badgeRepository;
    private final Handler<Badge, BadgeRepository> badgeBadgeRepositoryHandler;
    private final ModelMapper modelMapper;

    public Optional<Badge> getBadgeById(Long badgeId) {
        return badgeRepository.findById(badgeId);
    }

    public Page<Badge> getBadgesAttachedToUser(Long userId, Pageable pageable) {
        return badgeRepository.findBadgesByUsersId(userId, pageable);
    }

    public Page<Badge> getBadges(Pageable pageable) {
        return badgeRepository.findAll(pageable);
    }

    public Badge save(Badge badge) {
        return badgeBadgeRepositoryHandler.trySave(badge, badgeRepository);
    }

    public Badge update(Long badgeId, Badge badge) {
        var badgeToUpdate = getBadge(badgeId);
        modelMapper.map(badge, badgeToUpdate);
        return badgeToUpdate;
    }

    public void delete(Long badgeId) {
        getBadge(badgeId).deleteBadgeFromUsers();
        badgeRepository.deleteById(badgeId);
    }

    private Badge getBadge(Long badgeId) {
        return getBadgeById(badgeId)
                .orElseThrow(() -> new NotFoundException("Nie znalezino podanej odznaki"));
    }
}
