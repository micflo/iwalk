package com.bus.timetable.badge.service;

import com.bus.timetable.badge.model.Badge;
import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.BiConsumer;

@Service
@Transactional
@AllArgsConstructor
public class AdvanceBadgeService {

    private final UserService userService;
    private final BadgeService badgeService;


    public void attachBadgeToUser(Long badgeId, Long userId) {
        makeOperation(badgeId, userId, ((badge, user) -> {
            badge.getUsers().add(user);
            user.getBadges().add(badge);
        }));
    }

    public void detachBadgeFromUser(Long badgeId, Long userId) {
        makeOperation(badgeId, userId, ((badge, user) -> {
            badge.getUsers().remove(user);
            user.getBadges().remove(badge);
        }));
    }

    private void makeOperation(Long badgeId, Long userId, BiConsumer<Badge, User> userBadgeBiConsumer) {
        var badge = badgeService.getBadgeById(badgeId)
                .orElseThrow(() -> new NotFoundException("Nie znalezino podanej odznaki"));

        var user = userService.getUserById(userId)
                .orElseThrow(() -> new NotFoundException("Nie znaleziono podanego użytkownika."));

        userBadgeBiConsumer.accept(badge, user);
    }
}
