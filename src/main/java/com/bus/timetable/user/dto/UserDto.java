package com.bus.timetable.user.dto;


import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.Date;

@Getter
@Setter
public class UserDto {

    private Long id;
    private String username;
    private OffsetDateTime created;
    private OffsetDateTime stamp;
    private String name;
    private String lastname;
    private String mail;
    private Date dateOfBirth;
    private Boolean enabled;
}
