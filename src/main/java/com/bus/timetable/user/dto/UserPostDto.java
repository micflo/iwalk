package com.bus.timetable.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserPostDto {
    private String username;
    private String name;
    private String lastname;
    private String password;
    private String mail;
    private Date dateOfBirth;
}
