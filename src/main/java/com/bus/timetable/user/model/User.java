package com.bus.timetable.user.model;

import com.bus.timetable.audit.DateAudit;
import com.bus.timetable.authority.model.Role;
import com.bus.timetable.badge.model.Badge;
import com.bus.timetable.tripHistory.model.TripHistory;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;


@Getter
@Setter
@Entity
@ToString(callSuper = true)
public class User extends DateAudit implements UserDetails {

    @Id
    @GeneratedValue
    private Long id;

    @NaturalId
    @Getter(AccessLevel.NONE)
    private String username;

    @NotNull
    @Length(max = 20)
    private String name;

    @NotNull
    @Length(max = 30)
    private String lastname;

    @NotNull
    @Length(min = 7)
    private String password;

    @NaturalId(mutable = true)
    @NotNull
    @Length(max = 50)
    @Email
    private String mail;

    @NotNull
    private Date dateOfBirth;

    @Getter(AccessLevel.NONE)
    private Boolean enabled = false;

    @ManyToMany
    @JoinTable(
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "badge_id")
    )
    private Set<Badge> badges = new HashSet<>();

    @ManyToMany
    @JoinTable(
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles = new HashSet<>();

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<TripHistory> tripHistories = new ArrayList<>();

    @Formula("(SELECT COUNT(*) FROM USER_BADGES ub WHERE ub.USER_ID = id)")
    private Long badgesOfSize;

    @Formula("(SELECT COUNT(*) FROM TRIP_HISTORY th WHERE th.USER_ID = id)")
    private Long tripHistoriesOfSize;

    @Formula("(SELECT COUNT(*) FROM USER_ROLES ur WHERE ur.USER_ID = id)")
    private Long rolesOfSize;

    public void excludeUserFromBadges() {
        badges.forEach(badge -> badge.excludeUser(this));
    }

    public void excludeUserFromRoles() {
        roles.forEach(role -> role.getUsers().remove(this));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
