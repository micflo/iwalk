package com.bus.timetable.user.repository;

import com.bus.timetable.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> getUserByUsername(String username);
    boolean existsByUsername(String username);
    boolean existsByMail(String mail);
}
