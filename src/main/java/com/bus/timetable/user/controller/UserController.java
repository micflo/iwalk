package com.bus.timetable.user.controller;

import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.user.dto.UserDto;
import com.bus.timetable.user.dto.UserPostDto;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.service.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/users")
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    @GetMapping("{userId}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long userId) {
        return userService.getUserById(userId)
                .map(user -> modelMapper.map(user, UserDto.class))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NotFoundException("Podany użytkownik nie istnieje."));
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long userId) {
        userService.delete(userId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("registration")
    public ResponseEntity<UserDto> createUser(@RequestBody UserPostDto userPostDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(modelMapper.map(userService.save(modelMapper.map(userPostDto, User.class)), UserDto.class));
    }

    @GetMapping
    public ResponseEntity<Page<UserDto>> getUsers(Pageable pageable) {
        return ResponseEntity.ok(userService.getUsers(pageable)
                .map(user -> modelMapper.map(user, UserDto.class)));
    }
}
