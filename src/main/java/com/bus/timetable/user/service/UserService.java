package com.bus.timetable.user.service;

import com.bus.timetable.exception.MailExistException;
import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.exception.UserExistError;
import com.bus.timetable.exception.service.Handler;
import com.bus.timetable.user.model.User;
import com.bus.timetable.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final Handler<User, UserRepository> userUserRepositoryHandler;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public Optional<User> getUserById(Long userId) {
        return userRepository.findById(userId);
    }

    public Optional<User> getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    public User save(User user) {
        validCreatingAccount(user);
        user.setEnabled(true);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userUserRepositoryHandler.trySave(user, userRepository);
    }

    public void delete(Long userId) {
        var user = getUserById(userId)
                .orElseThrow(() -> new NotFoundException("Nie znaleziono podanego użytkownika."));
        user.excludeUserFromBadges();
        user.excludeUserFromRoles();

        userRepository.delete(user);
    }

    public Page<User> getUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }
    
    private void validCreatingAccount(User user) {
        String mail = user.getMail();
        if (userRepository.existsByMail(mail)) {
            throw new MailExistException(mail);
        }
        
        String username = user.getUsername();
        if (userRepository.existsByUsername(username)) {
            throw new UserExistError(username);
        }
    }
}
