package com.bus.timetable.configuration;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    @Scope("prototype")
    public ModelMapper modelMapper() {
        var modelMapper = new ModelMapper();
        var configuration = modelMapper.getConfiguration();
        configuration.setSkipNullEnabled(true);
        configuration.setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper;
    }
}
