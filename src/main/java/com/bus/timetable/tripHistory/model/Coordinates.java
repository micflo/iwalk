package com.bus.timetable.tripHistory.model;

import com.bus.timetable.audit.DateAudit;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
@ToString(callSuper = true)
public class Coordinates extends DateAudit {

    @Id
    private Long id;

    private Double latitude;
    private Double longitude;

    @ManyToOne
    private TripHistory tripHistory;
}
