package com.bus.timetable.tripHistory.model;

import com.bus.timetable.audit.DateAudit;
import com.bus.timetable.user.model.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class TripHistory extends DateAudit {

    @Id
    @GeneratedValue
    private Long id;

    private OffsetDateTime startDate;

    private OffsetDateTime endDate;

    private Long duration;

    private Long distance;

    private Long elevation;

    private Boolean isFinished = false;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "tripHistory", orphanRemoval = true, cascade = {CascadeType.PERSIST})
    private Set<Coordinates> coordinates = new HashSet<>();
}
