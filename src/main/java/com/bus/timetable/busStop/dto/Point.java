package com.bus.timetable.busStop.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Point {
    private float latitude;
    private float longitude;
}
