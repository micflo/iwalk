package com.bus.timetable.busStop.dto;

import com.bus.timetable.busStop.model.BusStop;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.neo4j.annotation.QueryResult;

@ToString
@Getter
@Setter
@QueryResult
public class StartStopBusStop {

    private float startBusStopDistance;
    private BusStop startBusStop;
    private float endBusStopDistance;
    private BusStop endBusStop;
}


