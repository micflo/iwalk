package com.bus.timetable.busStop.service;

import com.bus.timetable.busStop.model.BusStopSegment;
import com.bus.timetable.busStop.repository.BusStopSegmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BusStopSegmentService {

    private final BusStopSegmentRepository busStopSegmentRepository;
    private final BusStopService busStopService;


    public void createSegmentFromBusStops(long startBusStop, long endBusStop) {
        BusStopSegment busStopSegment = new BusStopSegment();
        busStopService.getBusStopById(startBusStop)
                .ifPresent(busStopSegment::setBusStop);

        busStopService.getBusStopById(endBusStop)
                .ifPresent(busStopSegment::setNextBusStop);

        String sourceBusStopName = busStopSegment.getBusStop().getName();
        String destinationBusStopName = busStopSegment.getNextBusStop().getName();
        busStopSegment.setRouteName(sourceBusStopName + "-" + destinationBusStopName);
        busStopSegmentRepository.save(busStopSegment);
    }

}
