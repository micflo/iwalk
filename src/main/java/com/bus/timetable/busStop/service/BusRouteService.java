package com.bus.timetable.busStop.service;

import com.bus.timetable.busStop.model.BusRoute;
import com.bus.timetable.busStop.repository.BusRouteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class BusRouteService {

    private final BusRouteRepository busRouteRepository;

    public BusRoute createBusRoute(BusRoute busRoute) {
        return busRouteRepository.save(busRoute);
    }
}
