package com.bus.timetable.busStop.service;

import com.bus.timetable.busStop.model.BusStop;
import com.bus.timetable.busStop.dto.Point;
import com.bus.timetable.busStop.dto.StartStopBusStop;
import com.bus.timetable.busStop.repository.BusStopRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class BusStopService {

    private final BusStopRepository busStopRepository;


    public BusStop save(BusStop busStop) {
        return busStopRepository.save(busStop);
    }

    public List<BusStop> findBusStopsBetweenTwoBusStops(Long startBusStopId, Long endBusStopId) {
        return busStopRepository.findBusStopsBetweenTwoBusStops(startBusStopId, endBusStopId);
    }

    public Optional<StartStopBusStop> getTwoNearestBusStopBaseOneStartAndStopPoint(Point startPoint, Point endPoint) {
        return busStopRepository.findTwoNearestBusStopBaseOneStartAndStopPoint(startPoint, endPoint);
    }

    public Optional<BusStop> getBusStopById(long busStopId) {
        return busStopRepository.findById(busStopId);
    }

    public void delete(long busStopId) {
        busStopRepository.deleteById(busStopId);
    }
}
