package com.bus.timetable.busStop.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
@Getter
@Setter
@ToString
public class BusStop {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private float latitude;
    private float longitude;

    @Relationship(value = "NEXT_BUS_STOP")
    @ToString.Exclude
    private BusStopSegment busStopSegment;
}
