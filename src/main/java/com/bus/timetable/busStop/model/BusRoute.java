package com.bus.timetable.busStop.model;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.ArrayList;
import java.util.List;

@NodeEntity
@Setter
@Getter
public class BusRoute {

    @Id
    @GeneratedValue
    private Long id;

    private String line;

    @Relationship(type = "BELONGS_TO")
    private List<BusStop> busStops = new ArrayList<>();

}
