package com.bus.timetable.busStop.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.*;

@RelationshipEntity(type = "NEXT_BUS_STOP")
@Setter
@Getter
@ToString
public class BusStopSegment {

    @Id
    @GeneratedValue
    private Long id;

    @StartNode
    private BusStop busStop;

    @EndNode
    private BusStop nextBusStop;

    private String routeName;
}
