package com.bus.timetable.busStop.repository;

import com.bus.timetable.busStop.model.BusRoute;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface BusRouteRepository extends Neo4jRepository<BusRoute, Long> {
}
