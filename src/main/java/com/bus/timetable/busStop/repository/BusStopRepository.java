package com.bus.timetable.busStop.repository;

import com.bus.timetable.busStop.model.BusStop;
import com.bus.timetable.busStop.dto.Point;
import com.bus.timetable.busStop.dto.StartStopBusStop;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BusStopRepository extends Neo4jRepository<BusStop, Long> {

    @Query("MATCH(startBusStop:BusStop),(stopBusStop:BusStop), sp = shortestPath((startBusStop)-[:NEXT_BUS_STOP*]->(stopBusStop)) " +
            "WHERE ID(startBusStop) = $start AND ID(stopBusStop) = $end " +
            "RETURN sp")
    List<BusStop> findBusStopsBetweenTwoBusStops(@Param("start") Long start, @Param("end") Long end);

    @Query("MATCH (start:BusStop)-[:NEXT_BUS_STOP]-> (end:BusStop)\n" +
            "        WITH\n" +
            "        min(round(distance(point({longitude: start.longitude, latitude: start.latitude}), point($startPoint)))) as minStartRouteDistance,\n" +
            "        min(round(distance(point({longitude: end.longitude, latitude: end.latitude}), point($endPoint)))) as minEndRouteDistance\n" +
            "        MATCH (start:BusStop)-[:NEXT_BUS_STOP]->(end:BusStop)\n" +
            "        WITH\n" +
            "        round(distance(point({longitude: start.longitude, latitude: start.latitude}), point($startPoint)))  as startBusStopDistance,\n" +
            "        start as startBusStop,\n" +
            "        round(distance(point({longitude: end.longitude, latitude: end.latitude}), point($endPoint)))  as endBusStopDistance,\n" +
            "        end as endBusStop\n" +
            "        WHERE startBusStopDistance = minStartRouteDistance\n" +
            "        AND  endBusStopDistance = minEndRouteDistance\n" +
            "        RETURN startBusStopDistance, startBusStop, endBusStopDistance, endBusStop")
    Optional<StartStopBusStop> findTwoNearestBusStopBaseOneStartAndStopPoint(@Param("startPoint") Point startPoint, @Param("endPoint") Point endPoint);
}


