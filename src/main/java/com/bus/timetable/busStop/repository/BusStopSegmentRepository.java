package com.bus.timetable.busStop.repository;

import com.bus.timetable.busStop.model.BusStopSegment;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface BusStopSegmentRepository extends Neo4jRepository<BusStopSegment, Long> {

//    Iterable<BusStopSegment> findAllByBusStop_IdAndNextBusStop_Id(Long start, Long end);
}
