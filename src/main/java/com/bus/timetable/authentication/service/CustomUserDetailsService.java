package com.bus.timetable.authentication.service;

import com.bus.timetable.exception.NotFoundException;
import com.bus.timetable.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var internalUser = userService.getUserByUsername(username)
                .orElseThrow(() -> new NotFoundException("Podany użytkownik: " + username + " nie istnieje."));

        return new User(internalUser.getUsername(), internalUser.getPassword(), internalUser.getAuthorities());
    }
}
