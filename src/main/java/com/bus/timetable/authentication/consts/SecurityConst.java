package com.bus.timetable.authentication.consts;

public class SecurityConst {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME_10_DAYS = 864_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER = "Authorization";
    public static final String REGISTRATION_URL = "/api/v1/users/registration";
    public static final String ROUTE_URL = "/api/v1/routes";
    public static final String ROUTE_BY_BUS_STOP_URL = "/api/v1/bus-stop/routes";
    public static final String SWAGGER_URL = "/swagger-ui.html/**";
    public static final String PERMIT_ALL = "**";
}
