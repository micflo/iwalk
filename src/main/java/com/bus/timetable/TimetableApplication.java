package com.bus.timetable;

import com.bus.timetable.busStop.repository.BusRouteRepository;
import com.bus.timetable.busStop.repository.BusStopRepository;
import com.bus.timetable.busStop.repository.BusStopSegmentRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@SpringBootApplication
@EnableNeo4jRepositories
@EnableJpaRepositories(excludeFilters =
@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = {BusStopRepository.class, BusRouteRepository.class, BusStopSegmentRepository.class}))
public class TimetableApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimetableApplication.class, args);
	}
}
