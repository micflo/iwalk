package com.bus.timetable.exception.service;

import com.bus.timetable.exception.NotNullException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.PropertyValueException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class Handler<T, U extends JpaRepository<T , Long>> {

    public T trySave(T entity, U jpaRepository) {
        try {
            return jpaRepository.saveAndFlush(entity);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof PropertyValueException) {
                var fieldName = ((PropertyValueException) e.getCause()).getPropertyName();
                log.error("Required field '{}' is empty", fieldName);
                throw new NotNullException(fieldName);
            }
            throw e;
        }
    }
}
