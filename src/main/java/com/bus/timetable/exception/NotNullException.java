package com.bus.timetable.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotNullException extends RuntimeException {

    public NotNullException(String fieldName) {
        super("Pole " + fieldName + " nie może być puste.");
    }
}
