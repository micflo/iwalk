package com.bus.timetable.exception;

public class MailExistException extends RuntimeException {

    public MailExistException(String mail) {
        super("Podany email: " + mail + " już istnieje.");
    }
}
