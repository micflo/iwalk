package com.bus.timetable.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserExistError extends RuntimeException {

    public UserExistError(String username) {
        super("Podana nazwa użytkownika: " + username + " już istnieje.");
    }
}
