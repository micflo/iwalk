package com.bus.timetable.route.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Step {

    private float distance;
    private float duration;
    private Geometry geometry;
}
