package com.bus.timetable.route.model;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@Builder
public class RequestSearchModel {

    private float startLng;
    private float startLat;
    private float endLng;
    private float endLat;

    private static final String SEARCH_OPTION = "overview=false&&steps=true&geometries=geojson";

    public String getRequestUrl() {
        return String.format("%s,%s;%s,%s?%s", startLng, startLat, endLng, endLat, SEARCH_OPTION);
    }
}
