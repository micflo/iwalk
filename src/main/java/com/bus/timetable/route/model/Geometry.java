package com.bus.timetable.route.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Geometry {
    private List<List<Float>> coordinates;
}
