package com.bus.timetable.route.service;

import com.bus.timetable.route.model.RequestSearchModel;
import com.bus.timetable.route.model.RouteResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class RouteService {


    private static final String SEARCH_ENGINE_URL = "http://127.0.0.1:5000/route/v1/foot/";
    private final WebClient webClient;

    public RouteService(WebClient webClient) {
        this.webClient = webClient;
    }

    public RouteResponse searchRoutes(RequestSearchModel requestSearchModel) {
        return webClient.get()
                .uri(SEARCH_ENGINE_URL + requestSearchModel.getRequestUrl())
                .retrieve()
                .bodyToMono(RouteResponse.class)
                .block();
    }
}
