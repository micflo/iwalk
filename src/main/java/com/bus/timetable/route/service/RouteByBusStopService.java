package com.bus.timetable.route.service;

import com.bus.timetable.busStop.dto.Point;
import com.bus.timetable.busStop.dto.StartStopBusStop;
import com.bus.timetable.busStop.model.BusStop;
import com.bus.timetable.busStop.service.BusStopService;
import com.bus.timetable.route.model.RequestSearchModel;
import com.bus.timetable.route.model.RouteResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RouteByBusStopService {

    private final RouteService routeService;
    private final BusStopService busStopService;


    public List<RouteResponse> getRoute(RequestSearchModel requestSearchModel) {
        RouteResponse routeResponse = routeService.searchRoutes(requestSearchModel);

        float distanceOnFoot = routeResponse.getRoutes().get(0).getDistance();

        Point startPoint = new Point();
        startPoint.setLongitude(requestSearchModel.getStartLng());
        startPoint.setLatitude(requestSearchModel.getStartLat());
        Point endPoint = new Point();
        endPoint.setLongitude(requestSearchModel.getEndLng());
        endPoint.setLatitude(requestSearchModel.getEndLat());

        Optional<StartStopBusStop> twoNearestBusStopBaseOneStartAndStopPoint = busStopService.getTwoNearestBusStopBaseOneStartAndStopPoint(startPoint, endPoint);
        List<RouteResponse> routeResponses = new ArrayList<>();
        if (twoNearestBusStopBaseOneStartAndStopPoint.isPresent()) {
            StartStopBusStop startStopBusStop = twoNearestBusStopBaseOneStartAndStopPoint.get();
            float sumOfDistanceToTwoBusStops = startStopBusStop.getStartBusStopDistance() + startStopBusStop.getEndBusStopDistance();

            if (sumOfDistanceToTwoBusStops < distanceOnFoot) {
                BusStop startBusStop = startStopBusStop.getStartBusStop();
                RequestSearchModel requestSearchModelToStartBusStop = RequestSearchModel.builder()
                        .startLat(requestSearchModel.getStartLat())
                        .startLng(requestSearchModel.getStartLng())
                        .endLat(startBusStop.getLatitude())
                        .endLng(startBusStop.getLongitude())
                        .build();

                routeResponses.add(routeService.searchRoutes(requestSearchModelToStartBusStop));


                BusStop endBusStop = startStopBusStop.getEndBusStop();
                RequestSearchModel requestSearchModelToEndBusStop = RequestSearchModel.builder()
                        .startLat(requestSearchModel.getEndLat())
                        .startLng(requestSearchModel.getEndLng())
                        .endLat(endBusStop.getLatitude())
                        .endLng(endBusStop.getLongitude())
                        .build();

                routeResponses.add(routeService.searchRoutes(requestSearchModelToEndBusStop));
            }
        }

        if(routeResponses.isEmpty()) {
            routeResponses.add(routeResponse);
        }
        return routeResponses;
    }
}
