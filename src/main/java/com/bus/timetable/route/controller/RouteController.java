package com.bus.timetable.route.controller;

import com.bus.timetable.route.model.RequestSearchModel;
import com.bus.timetable.route.model.RouteResponse;
import com.bus.timetable.route.service.RouteByBusStopService;
import com.bus.timetable.route.service.RouteService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.bus.timetable.authentication.consts.SecurityConst.ROUTE_BY_BUS_STOP_URL;
import static com.bus.timetable.authentication.consts.SecurityConst.ROUTE_URL;

@RestController
@AllArgsConstructor
public class RouteController {

    private final RouteService routeService;
    private final RouteByBusStopService routeByBusStopService;

    @GetMapping(ROUTE_URL)
    @CrossOrigin
//  TODO In future remove cross origin problem, temporary solution
    public ResponseEntity<RouteResponse> getRoutes(RequestSearchModel requestSearchModel) {
        return ResponseEntity
                .ok(routeService.searchRoutes(requestSearchModel));
    }

    @GetMapping(ROUTE_BY_BUS_STOP_URL)
    @CrossOrigin
    public List<RouteResponse> getRoutesByBusStop(RequestSearchModel requestSearchModel) {
        return routeByBusStopService.getRoute(requestSearchModel);
    }
}