export interface Geometry {
    coordinates: number[][]
}

export interface Step {
    distance: number
    duration: number
    geometry: Geometry
}

export interface Leg {
    steps: Step[]
}


export interface Route {
    distance: number
    duration: number
    legs: Leg[]
}

export interface Tour {
    code: string
    routes: Route[]
}