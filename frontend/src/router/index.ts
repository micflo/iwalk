import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '@/views/Home.vue'
import Login from "@/views/Login.vue";
import Registration from "@/views/Registration.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      auth: false
    }
  },
  {
    path: '/registration',
    name: 'Registration',
    meta: {
      auth: false
    },
    component: Registration
  },
]

const router = new VueRouter({
  routes
})

export default router
