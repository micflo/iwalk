import axios from 'axios'
import {Location} from "@/components/map/model/location";
import {Tour} from "@/components/map/model/route";

export const routeApi = axios.create({
    baseURL: 'http://localhost:8080/api/v1'
})


export async function getRoutes(startLocation: Location, endLocation: Location): Promise<Tour[]> {
    const response = await routeApi.get('bus-stop/routes', {
        params: {
            startLng: startLocation.lng,
            startLat: startLocation.lat,
            endLng: endLocation.lng,
            endLat: endLocation.lat
        }
    })
    return response.data
}
